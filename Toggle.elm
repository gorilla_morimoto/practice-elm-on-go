module Toggle exposing (..)

import Html exposing (Html, button, div, program, text)
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)


--Main


main : Program Never Model Msg
main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



--Model


type alias Model =
    Bool


init : ( Model, Cmd Msg )
init =
    ( False, Cmd.none )



--Messages


type Msg
    = Expand
    | Collapse



--View


view : Model -> Html Msg
view model =
    Html.main_ []
        [ div [ class "main-header" ]
            [ Html.h1 [] [ text "Elm with Go!" ] ]
        , toggle model
        ]


toggle : Model -> Html Msg
toggle model =
    if model then
        div []
            [ button [ onClick Collapse ] [ text "Collapse" ]
            , Html.p [] [ text "Click to Hide" ]
            ]
    else
        div []
            [ button [ onClick Expand ] [ text "Expand" ] ]



--Update


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Expand ->
            ( True, Cmd.none )

        Collapse ->
            ( False, Cmd.none )



--Subscriptions


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none
