module IncDec exposing (..)

import Html exposing (Html, button, div, main_, program, text)
import Html.Events exposing (onClick)


--main


main : Program Never Model Msg
main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



--model


type alias Model =
    Int


init : ( Model, Cmd Msg )
init =
    ( 0, Cmd.none )



--msg


type Msg
    = Increment Int
    | Decrement Int



--view


view : Model -> Html Msg
view model =
    main_ []
        [ div []
            [ button [ onClick (Increment 1) ] [ text "+" ]
            , Html.br [] []
            , Html.p [] [ text (toString model) ]
            , button [ onClick (Decrement -1) ] [ text "-" ]
            ]
        ]



--update


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Increment howMuch ->
            ( model + howMuch, Cmd.none )

        Decrement howMuch ->
            ( model + howMuch, Cmd.none )



--subsctiprions


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none
