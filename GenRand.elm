module GenRand exposing (..)

import Html exposing (Html, button, div, program, text)
import Html.Events exposing (onClick)
import Random


--main


main : Program Never Model Msg
main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = always Sub.none
        }



--model


type alias Model =
    Int


init : ( Model, Cmd Msg )
init =
    ( 0, Cmd.none )



--msg


type Msg
    = Roll
    | OnResult Int



--view


view : Model -> Html Msg
view model =
    Html.main_ []
        [ div
            []
            [ button [ onClick Roll ] [ text "Roll" ]
            , text (toString model)
            ]
        ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Roll ->
            ( model, Random.generate OnResult (Random.int 1 6) )

        OnResult res ->
            ( res, Cmd.none )
